import os
from typing import Optional


def get_env(env_name: str) -> Optional[str]:
    return os.environ.get(env_name)


def get_env_or_raise(env_name: str) -> str:
    value = get_env(env_name)
    if value is None or value == "":
        raise EnvironmentError(
            f"An environmental variable was expected with the name {env_name}"
        )
    return value


def get_env_or_default(env_name: str, default: str) -> str:
    value = get_env(env_name)
    if value is None or value == "":
        return default
    return value
