import requests

from app.config import backend_api


def postRequestRanking() -> bool:
    response = requests.post(
        url=f"{backend_api()}/rank",
        headers={"Accept": "application/json"},
    )
    return response.status_code >= 200 & response.status_code < 300
