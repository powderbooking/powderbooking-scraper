#  Copyright (c) 2021. Michael Kemna.

from typing import List

import requests
from bs4 import BeautifulSoup

from app.api.availabilityApi import (
    FlattenedBookingTargetsDto,
    getAvailabilityScrape,
    postAvailabilities,
)
from app.config import build_booking_base_url
from app.scrapers.monitoredScraper import MonitoredScraper


class BookingScraper(MonitoredScraper):
    @property
    def _url_base(self) -> str:
        return build_booking_base_url()

    def _get_scrape_targets(self) -> List[FlattenedBookingTargetsDto]:
        scrape = getAvailabilityScrape()
        targets = []
        for params in scrape:
            resorts = params.pop("targets", None)
            for resort in resorts:
                targets.append(
                    {
                        **resort,
                        **params,
                    }
                )
        return targets

    def _post_endpoint(self, *args, **kwargs) -> bool:
        return postAvailabilities(*args, **kwargs)

    def _request_json(self, url: str) -> requests.Response:
        """
        Request the url and ask for json as a response.
        """
        self.latest_request_response = requests.get(
            url=url,
            headers={
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36",
                "Accept-Language": "en-US, en;q=0.5",
            },
        )
        return self.latest_request_response

    def _parse_request(self) -> None:
        soup = BeautifulSoup(self.latest_request_response.content, "html.parser")
        try:
            hotels = soup.find_all("div", attrs={"data-testid": "property-card"})

            failures = 0
            for hotel in hotels:
                try:
                    name = hotel.find("div", attrs={"data-testid": "title"}).get_text(
                        strip=True
                    )
                    try:
                        ratingDiv = hotel.find(
                            "div", attrs={"data-testid": "review-score"}
                        )
                        rating = ratingDiv.find("div").get_text(strip=True)
                    except:
                        rating = ""

                    try:
                        price_text = (
                            hotel.find(
                                "span",
                                attrs={"data-testid": "price-and-discounted-price"},
                            )
                            .get_text(strip=True)
                            .replace("€\xa0", "")
                        )
                        price = int(price_text)
                    except:
                        price = ""

                    try:
                        hotel_url = hotel.find(
                            "a", attrs={"data-testid": "title-link"}
                        )["href"]
                    except:
                        hotel_url = ""

                    self.results.append(
                        {
                            "name": name,
                            "rating": rating,
                            "price": price,
                            "url": hotel_url,
                            "checkin": self.latest_resort["checkin"],
                            "checkout": self.latest_resort["checkout"],
                            "resort_id": self.latest_resort["id"],
                        }
                    )
                except:
                    failures += 1

            if failures > 0:
                print(
                    f"Resort {self.latest_resort_id} gave parsing error(s) for {failures} out of {len(hotels)} hotels"
                )

        except:
            print(
                f"Resort {self.latest_resort_id} parsing error while finding all hotels"
            )
