#  Copyright (c) 2021. Michael Kemna.
from abc import ABC

from circuitbreaker import CircuitBreakerError
from prometheus_client import Counter, Summary
from prometheus_client.registry import CollectorRegistry
from requests import Response

from app.scrapers.scraper import Scraper


class MonitoredScraper(Scraper, ABC):
    """
    This class is a simple wrapper around Scraper that adds monitoring with Prometheus.
    """

    collector_registry: CollectorRegistry
    scraper_summary: Summary
    request_counter: Counter
    database_counter: Counter
    circuitbreaker_counter: Counter

    def __init__(
        self,
        collector_registry: CollectorRegistry = CollectorRegistry(),
    ):
        super().__init__()

        self.collector_registry = collector_registry
        self.scraper_summary = Summary(
            name="scraper_duration_seconds",
            documentation="Duration of the scraping process",
            registry=self.collector_registry,
        )
        self.request_counter = Counter(
            name="http_request_response_code_total",
            documentation="HTTP responses from scraping",
            labelnames=["status_code"],
            registry=self.collector_registry,
        )
        self.database_counter = Counter(
            name="database_errors_total",
            documentation="Any errors received while accessing the database",
            registry=self.collector_registry,
        )
        self.circuitbreaker_counter = Counter(
            name="circuitbreaker_open_total",
            documentation="Keep track of the amount of times the circuit was opened",
            registry=self.collector_registry,
        )

    def _post_results(self) -> None:
        with self.database_counter.count_exceptions():
            super(MonitoredScraper, self)._post_results()

    def _request_json(self, url: str) -> Response:
        try:
            return super(MonitoredScraper, self)._request_json(url)
        finally:
            self.request_counter.labels(
                status_code=self.latest_request_response.status_code
            ).inc()

    def scrape(self) -> None:
        with self.scraper_summary.time():
            try:
                super(MonitoredScraper, self).scrape()
            except CircuitBreakerError:
                self.circuitbreaker_counter.inc()
