#  Copyright (c) 2021. Michael Kemna.

from datetime import datetime
from typing import List

from app.api.resortApi import ScrapeDto
from app.api.weatherApi import (
    WeatherPostDto,
    getWeatherScrape,
    postWeather,
    postWeathers,
)
from app.config import DATETIME_BACKEND_FORMAT, build_openweathermap_base_url

# TODO: make monitoring enabled through env variable
from app.scrapers.monitoredScraper import MonitoredScraper

# from app.scrapers.scraper import Scraper
from app.scrapers.utils import get_nested


class WeatherScraper(MonitoredScraper):
    @property
    def _url_base(self) -> str:
        return build_openweathermap_base_url()

    def _post_endpoint(self, dtos: List[WeatherPostDto]) -> bool:
        result = postWeathers(dtos=dtos)
        if result:
            return result

        print(f"{type(self).__name__} failed posting batch, attempting individual")
        for dto in dtos:
            print(f"{type(self).__name__} posting {dto['resort_id']}")
            result = postWeather(dto=dto)
            if not result:
                print(f"{type(self).__name__} failed posting {dto['resort_id']}")

        return True

    def _get_scrape_targets(self) -> List[ScrapeDto]:
        return getWeatherScrape()

    def _parse_request(self) -> None:
        req = self.latest_request_response.json()

        dt = datetime.fromtimestamp(req["dt"]) if "dt" in req else datetime.now()

        record: WeatherPostDto = {
            "resort_id": self.latest_resort_id,
            "dt": dt.strftime(DATETIME_BACKEND_FORMAT),
            "temperature_c": get_nested(req, "main.temp"),
            "wind_speed_kmh": get_nested(req, "wind.speed"),
            "wind_direction_deg": get_nested(req, "wind.deg"),
            "visibility_km": get_nested(req, "visibility"),
            "clouds_pct": get_nested(req, "clouds.all"),
            "snow_3h_mm": get_nested(req, "snow.3h"),
            "rain_3h_mm": get_nested(req, "rain.3h"),
        }

        self.results.append(record)
