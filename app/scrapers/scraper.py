#  Copyright (c) 2021. Michael Kemna.

import time
from abc import ABC, abstractmethod
from math import trunc
from random import randint
from typing import List

import requests
from circuitbreaker import CircuitBreakerError, circuit
from requests import RequestException, Response

from app.api.weatherApi import ScrapeDto
from app.config import (
    DELAY_ERROR,
    DELAY_MAX,
    DELAY_MIN,
    FAILURE_THRESHOLD,
    QUEUE_MAX,
)


class Scraper(ABC):
    """
    This class is used as a base for all other scrapers.
    It is an abstract class, as it requires the values for each specific api to
    function.

    The process can be described as follows:
    1 - Retrieve the records that need to be scraped
    2 - For each record, request the data and parse the results
    3 - Insert that data into the database in batches
    """

    results: List[dict]
    latest_request_response: Response
    latest_resort_id: int
    latest_resort: dict

    def __init__(
        self,
    ):
        self._initialize_or_reset_results()

    @staticmethod
    def _delay(secs: float) -> None:
        """
        Sleep for secs period to avoid throttling / DNSing the endpoint.
        """
        time.sleep(secs)

    @staticmethod
    def _delay_random() -> None:
        """
        Sleep for a randomized amount of time.
        """
        Scraper._delay(randint(trunc(DELAY_MIN * 10), trunc(DELAY_MAX * 10)) / 10)

    @property
    @abstractmethod
    def _url_base(self) -> str:
        """
        The base url of the api that we are scraping.
        """
        pass

    @abstractmethod
    def _post_endpoint(self, *args, **kwargs) -> bool:
        """
        The endpoints to post the results to.

        Returns true if success, false if failed.
        """
        pass

    @abstractmethod
    def _parse_request(self) -> None:
        """
        Parse the request and add the outcome to the results so it is inserted into
        the database.
        """
        pass

    @abstractmethod
    def _get_scrape_targets(self) -> List[ScrapeDto]:
        """
        Retrieve the targets that we are about to scrape
        """
        pass

    @circuit(failure_threshold=FAILURE_THRESHOLD, expected_exception=RequestException)
    def _get_scrape_targets_resilient(self) -> List[ScrapeDto]:
        return self._get_scrape_targets()

    def _get_scrape_targets_generator(self) -> List[ScrapeDto]:
        while True:
            resorts = self._get_scrape_targets_resilient()
            if not resorts:
                break

            for resort in resorts:
                yield resort

    def _post_results(self) -> None:
        """
        Insert the results into the API.
        """
        if len(self.results) > 0:
            if self._post_endpoint(self.results):
                print(f"{type(self).__name__} posted (latest) {self.latest_resort_id}")
            else:
                print(
                    f"{type(self).__name__} FAILED posting (latest) {self.latest_resort_id}"
                )

        self._initialize_or_reset_results()

    def _batch_post_results(self, queue_max: int = QUEUE_MAX) -> bool:
        """
        Insert into the database in batches to limit the amount of database connections.

        Note: this function does NOT ensure results are inserted to the database.
        Instead, it will only insert when a sufficient amount of results are available.

        :return: true if they are inserted, false if not.
        """
        if len(self.results) >= queue_max:
            self._post_results()
            return True
        return False

    def _initialize_or_reset_results(self) -> None:
        """
        Initialize or reset the results to prepare for scraping a batch of results.
        """
        self.results = []

    def _request_json(self, url: str) -> Response:
        """
        Request the url and ask for json as a response.
        """
        self.latest_request_response = requests.get(
            url=url,
            headers={"Accept": "application/json"},
        )
        return self.latest_request_response

    @circuit(failure_threshold=FAILURE_THRESHOLD, expected_exception=RequestException)
    def _request_json_resilient(self, url: str) -> None:
        """
        Request the url and ask for json as a response.
        This function raise an exception if failed.
        It will only attempt it x times, thereafter the circuit is opened.

        It throws the following errors:
        RequestException - something wrong with the request, circuit closed.
        CircuitBreakerError - requests failed too many times, circuit is open.
        """
        self._request_json(url)
        self.latest_request_response.raise_for_status()

    def _finalise(self, *args, **kwargs) -> bool:
        """
        Get's called when there are no more scrape targets
        """
        pass

    def _scrape_resort(self, url: str, delay_error: float = DELAY_ERROR) -> None:
        """
        Scrape the particular resort, requesting it from the API, parsing the result and possibly insert it into the database.
        """
        try:
            self._request_json_resilient(url)

            self._parse_request()

            self._batch_post_results()

            self._delay_random()
        except RequestException:
            # We might be getting throttled, so we add another delay
            # Note: we do NOT retry the previous url
            self._delay(delay_error)
        except CircuitBreakerError:
            print(f"The circuit was broken after {FAILURE_THRESHOLD}, breaking up..")
            raise

    def scrape(self) -> None:
        """
        Scrape the data from the weather apis and add it to the database.
        """
        try:
            for resort in self._get_scrape_targets_generator():
                self.latest_resort = resort
                self.latest_resort_id = resort["id"]
                self._scrape_resort(self._url_base.format(**resort))
        finally:
            # insert the remaining items into the database
            self._post_results()

        self._finalise()
