#  Copyright (c) 2021. Michael Kemna.

from datetime import date, datetime
from typing import List

from app.api.forecastApi import (
    ForecastPostDto,
    getForecastScrape,
    postForecast,
    postForecasts,
)
from app.api.rankApi import postRequestRanking
from app.api.resortApi import ScrapeDto
from app.config import build_weatherunlocked_base_url
from app.scrapers.monitoredScraper import MonitoredScraper


class ForecastScraper(MonitoredScraper):
    @property
    def _url_base(self) -> str:
        return build_weatherunlocked_base_url()

    def _get_scrape_targets(self) -> List[ScrapeDto]:
        return getForecastScrape()

    def _post_endpoint(self, dtos: List[ForecastPostDto]) -> bool:
        result = postForecasts(dtos=dtos)
        if result:
            return result

        print(f"{type(self).__name__} failed posting batch, attempting individual")
        for dto in dtos:
            print(f"{type(self).__name__} posting {dto['resort_id']}")
            result = postForecast(dto=dto)
            if not result:
                print(f"{type(self).__name__} failed posting {dto['resort_id']}")

        return True

    def _parse_request(self) -> None:
        req = self.latest_request_response.json()

        # I am calculating the aggregate data here because it will be requested often
        # So I don't have to do an expensive query every time somebody requests the data
        snow_week = rain_week = 0
        for day in req["Days"]:
            if "snow_total_mm" in day:
                snow_week += day["snow_total_mm"]
            if "rain_total_mm" in day:
                rain_week += day["rain_total_mm"]

        forecastDays = []
        for day_number, day in enumerate(req["Days"], start=0):
            record = {
                "resort_id": self.latest_resort_id,
                "date": str(datetime.strptime(day["date"], "%d/%m/%Y").date()),
                "timepoint": day_number,
                "temperature_max_c": day["temp_max_c"],
                "temperature_min_c": day["temp_min_c"],
                "rain_total_mm": day["rain_total_mm"],
                "snow_total_mm": day["snow_total_mm"],
                "prob_precip_pct": day["prob_precip_pct"],
                "wind_speed_max_kmh": day["windspd_max_kmh"],
                "windgst_max_kmh": day["windgst_max_kmh"],
            }
            forecastDays.append(record)

        firstDay = next(iter(forecastDays), None)
        firstDate = firstDay["date"] if firstDay else str(date.today())

        forecast = {
            "resort_id": self.latest_resort_id,
            "date": firstDate,
            "rain_week_mm": rain_week,
            "snow_week_mm": snow_week,
            "forecast_days": forecastDays,
        }

        self.results.append(forecast)

    def _finalise(self) -> bool:
        result = postRequestRanking()
        return result
