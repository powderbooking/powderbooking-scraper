#  Copyright (c) 2021. Michael Kemna.

from app.utils import get_env, get_env_or_default, get_env_or_raise

# https://docs.python.org/3/library/datetime.html#strftime-and-strptime-behavior
# 2023-06-18T16:34:56.470Z
DATETIME_BACKEND_FORMAT = "%Y-%m-%dT%H:%M:%SZ"
DATE_BACKEND_FORMAT = "%Y-%m-%d"

# See the Dockerfile for their default settings and explanation
DELAY_MIN = float(get_env_or_default("DELAY_MIN", "0.4"))
DELAY_MAX = float(get_env_or_default("DELAY_MAX", "1.0"))
DELAY_ERROR = float(get_env_or_default("DELAY_ERROR", "2.0"))
QUEUE_MAX = int(get_env_or_default("QUEUE_MAX", "20"))
FAILURE_THRESHOLD = int(get_env_or_default("FAILURE_THRESHOLD", "10"))
PROMETHEUS_PUSHGATEWAY = get_env("PROMETHEUS_PUSHGATEWAY")


def backend_api():
    return get_env_or_raise("BACKEND_API")


def wu_app_id():
    return get_env_or_raise("WEATHERUNLOCKED_APP_ID")


def wu_app_key():
    return get_env_or_raise("WEATHERUNLOCKED_APP_KEY")


def owm_app_id():
    return get_env_or_raise("OPENWEATHERMAP_APP_ID")


def build_weatherunlocked_base_url(localweathertype: str = "forecast") -> str:
    """
    Build the base url for a weatherunlocked request.
    Credentials are built from environmental variables.

    :param localweathertype: 'forecast' or 'current'
    :return: base url string that needs lat and lng params inserted
    """
    return (
        f"http://api.weatherunlocked.com/api/{localweathertype}/{{lat}},"
        f"{{lng}}?app_id={wu_app_id()}&app_key={wu_app_key()}"
    )


def build_openweathermap_base_url() -> str:
    """
    Build the base url for an openweathermap request.
    Credentials are built from environmental variables.

    :return: base url string that needs lat and lng params inserted
    """
    return (
        f"https://api.openweathermap.org/data/2.5/weather?lat={{lat}}&lon={{"
        f"lng}}&appid={owm_app_id()}&units=metric"
    )


def build_booking_base_url() -> str:
    """
    Build the base url for a Booking request.

    :return: base url string that needs booking params inserted
    """
    return (
        "https://www.booking.com/searchresults.en-gb.html?"
        "landmark={landmark}&order=price&aid=304142"
        "checkin={checkin}&checkout={checkout}&"
        "group_adults={adults}&no_rooms={rooms}&group_children={children}"
    )
