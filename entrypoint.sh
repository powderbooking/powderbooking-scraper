#!/bin/sh

#
# Copyright (c) 2021. Michael Kemna.
#

# exit when a command fails
set -e
# exit when script tries to use undeclared variables
set -u

args="$*"

# https://github.com/ufoscout/docker-compose-wait
/wait

echo "Starting app from entrypoint with arguments: $args"

exec python3 app.py $args