# powderbooking-scraper

Python scraper for the Powderbooking application.

## How to use

This application requires a postgres database to function.

#### Python

To run:
1. `poetry install --no-root`
2. set `.env` based on `.env.template`
3. the backend must run to be able to test the scraper
4. `poetry run python3 app.py --api [API]`
   - `[API]` is either forecast or weather

To test: `poetry run pytest`

To lint: `poetry run ruff check`

To format: `poetry run ruff format`

You are encouraged to [integrate ruff with your editor](https://docs.astral.sh/ruff/editors/setup/)

## Used APIs

These are the APIs that I am getting my data from.

**NOTE:**
All information with regards to the APIs is last retrieved and verified in summer 2018.

#### Open Weather Map

**Pricing:**   
free (up to 60/min calls)   
**url:** https://openweathermap.org/price    
**documentation:** https://openweathermap.org/current    
**Notes:**   
Includes rain and snow fall from last 3 hours (in mm)   
Updates every 2 hours

#### Weather unlocked

**Pricing:**    
7 days: free (up to 75/min calls)
**url:** https://developer.worldweatheronline.com/api/pricing.aspx    
**documentation:** https://developer.weatherunlocked.com/documentation/localweather/forecast      
**sample:** https://developer.worldweatheronline.com/api/docs/ski_sample.xml  
**Notes:**   
Could request multiple times per day    
3-hourly segment   
Updates every hour

## Unused APIs

These are APIs I have looked at, but currently not using.

#### World Weather Online

**Pricing:**   
7 days (5000/day): 28.73     
10 days (5000/day): 35.93   
14 days (5000/day): 43.13    
**url:** https://developer.worldweatheronline.com/api/pricing.aspx    
**documentation:** https://developer.worldweatheronline.com/api/docs/local-city-town-weather-api.aspx    
**Notes:**   
Would request it once per day       
Daily snowFall_mm Hourly precipMM and chanceofrain -snow -frost -windy -sunshine -fog

#### World Weather Online

**Pricing:**    
3 days (5000/day): 12.09   
5 days (5000/day): 20.19   
7 days (5000/day): 26.94   
**url:** https://developer.worldweatheronline.com/api/pricing.aspx    
**documentation:** https://developer.worldweatheronline.com/api/docs/ski-weather-api.aspx   
**sample:** https://developer.worldweatheronline.com/api/docs/ski_sample.xml  
**Notes:**   
Would request it once per day    
Hourly segment   
Includes top / mid / bottom   