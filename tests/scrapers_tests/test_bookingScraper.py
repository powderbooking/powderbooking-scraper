#  Copyright (c) 2021. Michael Kemna.

import datetime

from pytest_mock import MockerFixture

from app.scrapers.bookingScraper import BookingScraper


def test_parse_request(mock_envs, mocker: MockerFixture):
    # arrange
    sut = BookingScraper()
    dto = [
        {
            "id": 1,
            "landmark": 15295,
            "checkin": datetime.date.today() + datetime.timedelta(days=2),
            "checkout": datetime.date.today() + datetime.timedelta(days=9),
            "adults": 2,
            "children": 0,
            "rooms": 1,
        }
    ]
    sut._get_scrape_targets = mocker.Mock(side_effect=[dto, None])
    mockedPost = mocker.Mock(return_value=True)
    sut._post_endpoint = mockedPost

    # act
    sut.scrape()

    # assert
    mockedPost.assert_called_once()
