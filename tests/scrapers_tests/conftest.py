import pytest


@pytest.fixture
def mock_envs(monkeypatch):
    monkeypatch.setenv("BACKEND_API", "foo")
    monkeypatch.setenv("WEATHERUNLOCKED_APP_ID", "foo")
    monkeypatch.setenv("WEATHERUNLOCKED_APP_KEY", "foo")
    monkeypatch.setenv("OPENWEATHERMAP_APP_ID", "foo")
