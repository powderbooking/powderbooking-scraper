#  Copyright (c) 2021. Michael Kemna.

import plac
from prometheus_client.exposition import push_to_gateway
from prometheus_client.registry import CollectorRegistry

from app.config import PROMETHEUS_PUSHGATEWAY
from app.scrapers.bookingScraper import BookingScraper
from app.scrapers.forecastScraper import ForecastScraper
from app.scrapers.scraper import Scraper
from app.scrapers.weatherScraper import WeatherScraper


@plac.opt("api", "The API to scrape, can be forecast or weather")
def main(api: str = "forecast"):
    """
    Scrape either the weather or forecast API and insert the results into the database.
    """
    print("Creating database Handler.")
    collector_registry = CollectorRegistry()

    print(f"Initiating scraping session for api: {api}.")
    scraper = scraper_builder(api, collector_registry)
    scraper.scrape()

    print("Finished scraping")

    if PROMETHEUS_PUSHGATEWAY:
        print("Attempting to push to gateway")
        try:
            push_to_gateway(
                gateway=PROMETHEUS_PUSHGATEWAY, job=api, registry=collector_registry
            )
            print("Pushed to gateway successfully.")
        except:
            print("Unable to push to gateway, is it configured correctly?")


def scraper_builder(api: str, collector_registry: CollectorRegistry) -> Scraper:
    if api == "forecast":
        return ForecastScraper(collector_registry=collector_registry)
    elif api == "weather":
        return WeatherScraper(collector_registry=collector_registry)
    elif api == "booking":
        return BookingScraper(collector_registry=collector_registry)
    else:
        raise ValueError(
            "Invalid input for api, can only be of type 'forecast' or 'weather'."
        )


if __name__ == "__main__":
    plac.call(main)
